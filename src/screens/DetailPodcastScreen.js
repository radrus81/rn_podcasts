import React from "react";
import {
  StyleSheet,
  Image,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { AppHeaderIcon } from "../components/ui/AppHeaderIcon";
import { AppText } from "../components/ui/AppText";
import { TitleDetailPodcast } from "../components/DetailPodcastScreen/TitleDetailPodcast";
import { ButtonListenPodcast } from "../components/DetailPodcastScreen/ButtonListenPodcast";

export const DetailPodcastScreen = ({ navigation }) => {
  const namePodcast = navigation.getParam("topic");
  const detailPodcast = navigation.getParam("detailPodcast");

  console.log(detailPodcast);

  return (
    <ScrollView style={styles.wrapper} showsVerticalScrollIndicator={false}>
      <TitleDetailPodcast
        namePodcast={namePodcast}
        title={detailPodcast.title}
        time={detailPodcast.time}
      />
      <ButtonListenPodcast />
      <AppText>{detailPodcast.description}</AppText>
    </ScrollView>
  );
};

DetailPodcastScreen.navigationOptions = ({ navigation }) => {
  return {
    headerTitle: () => <AppText style={styles.mainHeaderTitle}>Серия</AppText>,
    headerLeft: () => (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => navigation.navigate("Main")}
      >
        <Image
          source={require("../../assets/images/logo.jpg")}
          style={styles.logo}
        />
      </TouchableOpacity>
    ),
    headerRight: () => (
      <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
        <Item
          title="md-more"
          iconName="md-more"
          onPress={() => navigation.push("Setting")}
        />
      </HeaderButtons>
    ),
  };
};

const styles = StyleSheet.create({
  mainHeaderTitle: {
    width: "100%",
    color: "#fff",
    fontSize: 20,
  },
  logo: {
    width: 25,
    height: 25,
    borderRadius: 50,
    marginLeft: 15,
  },
  headerTitle: {
    color: "#fff",
    fontSize: 20,
  },
  wrapper: {
    margin: 20,
    marginTop: 30,
  },
});
