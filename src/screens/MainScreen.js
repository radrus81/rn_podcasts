import React, { useEffect } from "react";
import { StyleSheet, ScrollView, Image, View } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useDispatch, useSelector } from "react-redux";
import { AppHeaderIcon } from "../components/ui/AppHeaderIcon";
import { WrapperPodcast } from "../components/WrapperPodcast";
import { loadTopPodcasts } from "../store/action/podcastAction";
import { InfoPanel } from "../components/InfoPanel/InfoPanel";
import { AppText } from "../components/ui/AppText";


export const MainScreen = ({ navigation }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadTopPodcasts());
  }, [dispatch]);

  const topPodcasts = useSelector((state) => state.podcast.topPodcasts);

  const openPodcastHandler = (id) => {
    navigation.navigate("Podcast", { idPodcast: id });
  };

  return (
    <ScrollView>
      <InfoPanel />
      <WrapperPodcast
        title="Топ подкастов"
        dataPodcasts={topPodcasts}
        onOpen={openPodcastHandler}
      />
      <WrapperPodcast
        title="Популярные подкасты"
        dataPodcasts={topPodcasts}
        onOpen={openPodcastHandler}
      />
    </ScrollView>
  );
};

MainScreen.navigationOptions = ({ navigation }) => ({
  headerTitle: () => (
    <View style={styles.headerTitle}>
      <Image
        source={require("../../assets/images/logo.jpg")}
        style={styles.image}
      />
      <AppText style={styles.title}>Подкасты</AppText>
    </View>
  ),
  headerRight: () => (
    <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
      <Item
        title="ios-search"
        iconName="ios-search"
        onPress={() => navigation.toggleDrawer()}
      />
      <Item
        title="md-more"
        iconName="md-more"
        onPress={() => navigation.push("Setting")}
      />
    </HeaderButtons>
  ),
  headerLeft: () => (
    <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
      <Item
        title="ios-menu"
        iconName="ios-menu"
        onPress={() => navigation.toggleDrawer()}
      />
    </HeaderButtons>
  ),
});

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    color: "#fff",
    marginLeft: 5,
  },
  version: {
    fontFamily: "open-bold",
  },
  headerTitle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    width: 25,
    height: 25,
    borderRadius: 50,
  },
});
