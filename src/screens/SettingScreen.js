import React from "react";
import { StyleSheet, Image, View } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { AppHeaderIcon } from "../components/ui/AppHeaderIcon";
import { AppText } from "../components/ui/AppText";

export const SettingScreen = ({ }) => {
  return (
    <View style={styles.center}>
      <AppText>Тут будут настройки</AppText>
    </View>
  );
};

SettingScreen.navigationOptions = ({ navigation }) => ({
  headerTitle: () => (
    <View style={styles.headerTitle}>
      <Image source={require("../../assets/images/logo.jpg")} style={styles.image} />
      <AppText style={styles.title}>Настройки</AppText>
    </View>
  ),
  headerLeft: () => (
    <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
      <Item
        title="ios-menu"
        iconName="ios-menu"
        onPress={() => navigation.toggleDrawer()}
      />
    </HeaderButtons>
  ),

});

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    color: "#fff",
    marginLeft: 5
  },
  version: {
    fontFamily: "open-bold",
  },
  headerTitle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  image: {
    width: 25,
    height: 25,
    borderRadius: 50
  },
  version: {
    fontFamily: "open-bold",
  },
});

