import React from "react";
import { StyleSheet, Image, View } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { AppHeaderIcon } from "../components/ui/AppHeaderIcon";
import { AppText } from "../components/ui/AppText";
import { AppTextBold } from "../components/ui/AppTextBold";

export const AboutScreen = ({ }) => {
  return (
    <View style={styles.center}>
      <AppText>Это лучшее приложение для подкастов</AppText>
      <AppTextBold > Версия приложения 1.0.0</AppTextBold>
    </View>
  );
};

AboutScreen.navigationOptions = ({ navigation }) => ({
  headerTitle: () => (
    <View style={styles.headerTitle}>
      <Image source={require("../../assets/images/logo.jpg")} style={styles.image} />
      <AppText style={styles.title}>О приложении</AppText>
    </View>
  ),
  headerLeft: () => (
    <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
      <Item
        title="ios-menu"
        iconName="ios-menu"
        onPress={() => navigation.toggleDrawer()}
      />
    </HeaderButtons>
  ),

});

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    color: "#fff",
    marginLeft: 5
  },
  version: {
    fontFamily: "open-bold",
  },
  headerTitle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  image: {
    width: 25,
    height: 25,
    borderRadius: 50
  }
});

