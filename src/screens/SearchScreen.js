import React from "react";
import { View, StyleSheet } from "react-native";
import { AppText } from "../components/ui/AppText";


export const SearchScreen = ({ }) => {
  return (
    <View style={styles.center}>
      <AppText>Поиск</AppText>
    </View>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  version: {
    fontFamily: "open-bold",
  },
});
