import React, { useEffect } from "react";
import {
  StyleSheet,
  Image,
  View,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useSelector } from "react-redux";
import { AppHeaderIcon } from "../components/ui/AppHeaderIcon";
import { AppText } from "../components/ui/AppText";
import { TitlePodcast } from "../components/PodcastScreen/TitlePodcast";
import { SubscribeButton } from "../components/PodcastScreen/SubscribeButton";
import { DescriptPodcast } from "../components/PodcastScreen/DescriptPodcast";
import { ReleasesPodcast } from "../components/PodcastScreen/ReleasesPodcast";

export const PodcastScreen = ({ navigation }) => {
  const topPodcasts = useSelector((state) => state.podcast.topPodcasts);

  const idPodcast = navigation.getParam("idPodcast");
  const { topic, author, img, description, releases } = topPodcasts.find(
    (p) => p.id === idPodcast
  );

  const opacity = 0;

  useEffect(() => {
    navigation.setParams({ topic });
    navigation.setParams({ opacity });
  }, [topic, opacity]);

  const handleScroll = (event) => {
    let opacity;
    event.nativeEvent.contentOffset.y > 35
      ? (opacity = event.nativeEvent.contentOffset.y / 80)
      : (opacity = 0);

    navigation.setParams({ opacity });
  };

  const showDetailPodcast = (id) => {
    const detailPodcast = releases.find((cast) => cast.id === id);
    navigation.push("DetailPodcast", { topic, detailPodcast });
  };

  return (
    <ScrollView
      style={styles.wrapper}
      onScroll={handleScroll}
      scrollEventThrottle={5}
    >
      <TitlePodcast topic={topic} author={author} img={img} />
      <SubscribeButton />
      <DescriptPodcast description={description} />
      <ReleasesPodcast
        releases={releases}
        showDetailPodcast={showDetailPodcast}
      />
    </ScrollView>
  );
};

PodcastScreen.navigationOptions = ({ navigation }) => {
  const titleHeader = navigation.getParam("topic");
  const opacity = navigation.getParam("opacity");

  return {
    headerTitle: () => (
      <View style={styles.mainHeaderTitle}>
        <AppText style={{ ...styles.headerTitle, opacity }}>
          {titleHeader}
        </AppText>
      </View>
    ),
    headerLeft: () => (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => navigation.navigate("Main")}
      >
        <Image
          source={require("../../assets/images/logo.jpg")}
          style={styles.logo}
        />
      </TouchableOpacity>
    ),
    headerRight: () => (
      <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
        <Item
          title="md-more"
          iconName="md-more"
          onPress={() => navigation.push("Setting")}
        />
      </HeaderButtons>
    ),
  };
};

const styles = StyleSheet.create({
  wrapper: {
    // margin: 20
  },
  mainHeaderTitle: {
    width: "100%",
  },
  logo: {
    width: 25,
    height: 25,
    borderRadius: 50,
    marginLeft: 15,
  },
  headerTitle: {
    color: "#fff",
    fontSize: 20,
  },
});
