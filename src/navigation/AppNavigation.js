import React from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { Platform } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import { createDrawerNavigator } from "react-navigation-drawer";

import { MainScreen } from "../screens/MainScreen";
import { AboutScreen } from "../screens/AboutScreen";
import { SearchScreen } from "../screens/SearchScreen";
import { SettingScreen } from "../screens/SettingScreen";
import { PodcastScreen } from "../screens/PodcastScreen";
import { THEME } from "../theme";
import { DetailPodcastScreen } from "../screens/DetailPodcastScreen";

const navigatorOptions = {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: Platform.OS === "android" ? THEME.MAIN_COLOR : "#fff",
    },
    headerTintColor: Platform.OS === "android" ? "#fff" : THEME.MAIN_COLOR,
  },
};

const MainNavigator = createStackNavigator(
  {
    Main: MainScreen,
    Podcast: PodcastScreen,
    DetailPodcast: DetailPodcastScreen
  },
  navigatorOptions
);

const SettingNavigator = createStackNavigator(
  {
    Setting: SettingScreen,
  },
  navigatorOptions
);

const AboutNavigator = createStackNavigator(
  {
    About: AboutScreen,
  },
  navigatorOptions
);

const AppNavigator = createDrawerNavigator(
  {
    Main: {
      screen: MainNavigator,
      navigationOptions: {
        drawerLabel: "Главная",
        drawerIcon: <Ionicons name="md-home" size={20} />,
      },
    },
    Setting: {
      screen: SettingNavigator,
      navigationOptions: {
        drawerLabel: "Настройки",
        drawerIcon: <Ionicons name="ios-settings" size={20} />,
      },
    },
    About: {
      screen: AboutNavigator,
      navigationOptions: {
        drawerLabel: "О приложени",
        drawerIcon: <Ionicons name="logo-rss" size={20} />,
      },
    },
  },
  {
    contentOptions: {
      activeTintColor: THEME.MAIN_COLOR,
      labelStyle: {
        fontFamily: "open-regular",
      },
    },
  }
);

export const AppNavigation = createAppContainer(AppNavigator);
