import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { podcastReducer } from "./reducers/podcastReducer";

const rootReducer = combineReducers({
  podcast: podcastReducer,
});

export default createStore(rootReducer, applyMiddleware(thunk));
