import * as FileSystem from "expo-file-system";
import { LOAD_TOP_PODCASTS } from "../types";
//import { DB } from "../../db";
import { DATA } from "../../data";


export const loadTopPodcasts = () => {
  return async (dispatch) => {
    //const posts = await DB.getPosts();
    dispatch({
      type: LOAD_TOP_PODCASTS,
      payload: DATA,
    });
  };
};

