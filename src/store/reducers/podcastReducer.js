import { LOAD_TOP_PODCASTS } from "../types";

const initialState = {
  topPodcasts: [],
  loading: true,
};

export const podcastReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_TOP_PODCASTS:
      return {
        ...state,
        topPodcasts: action.payload,
        loading: false,
      };


    default:
      return state;
  }
};
