import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
} from "react-native";

export const WrapperPodcast = ({ title, dataPodcasts, onOpen }) => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>{title}</Text>
      <FlatList
        data={dataPodcasts}
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        keyExtractor={(podcast) => podcast.id.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity activeOpacity={0.7} onPress={() => onOpen(item.id)}>
            <View style={styles.blockPodpact}>
              <Image source={{ uri: item.img }} style={styles.image} />
              <Text style={styles.topicPodcast}>{item.topic}</Text>
              <Text style={styles.author}>{item.author}</Text>
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    padding: 10,
    marginTop: 10,
  },
  title: {
    textAlign: "left",
    fontSize: 20,
    marginBottom: 5,
    fontFamily: "open-regular",
  },
  blockPodpact: {
    width: 110,
    marginTop: 10,
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 5,
  },
  topicPodcast: {
    fontFamily: "open-bold",
    fontSize: 14,
    marginTop: 10,
  },
  author: {
    fontFamily: "open-regular",
    fontSize: 12,
    marginTop: 10,
  },
});
