import React from 'react'
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { AntDesign } from '@expo/vector-icons'

export const AppButton = ({ children, onPress, style }) => {

    return (
        <TouchableOpacity onPress={onPress} activeOpacity={0.7}>
            <View style={{ ...styles.default, ...style }}>
                {children}
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    default: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 15,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around"
    }
})