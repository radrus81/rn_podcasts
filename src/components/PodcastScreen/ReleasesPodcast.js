import React, { useState } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { AppText } from "../ui/AppText";
import { AppTextBold } from "../ui/AppTextBold";
import { THEME } from "../../theme";

export const ReleasesPodcast = ({ releases, showDetailPodcast }) => {
  return (

    <View style={styles.wrapper}>
      <AppText style={styles.title}>Доступные выпуски</AppText>
      {releases.map((release) => {
        return (
          <View style={styles.trackBlock} key={release.id}>
            <TouchableOpacity onPress={() => showDetailPodcast(release.id)}>
              <View style={styles.track}>
                <AppTextBold style={styles.trackTitle}>
                  {release.title.length > 28
                    ? release.title.substr(0, 28) + "..."
                    : release.title}
                </AppTextBold>
                <AppText style={styles.time}>{release.time}</AppText>
              </View>
            </TouchableOpacity>
            <View style={styles.icon}>
              <MaterialCommunityIcons
                name="play-circle-outline"
                size={25}
                color="#303f9f"
              />
            </View>
          </View>
        );
      })}
    </View>

  );
};

const styles = StyleSheet.create({
  wrapper: {
    marginBottom: 20,
  },
  title: {
    marginTop: 20,
    marginLeft: 20,
    marginBottom: 10,
    fontSize: 22,
  },
  trackBlock: {
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
    flexDirection: "row",
    paddingBottom: 10,
    marginTop: 10,
  },
  track: {
    marginLeft: 20,
  },
  trackTitle: {
    marginBottom: 5,
    fontFamily: "open-bold",
  },
  icon: {
    flex: 1,
    alignItems: "flex-end",
    marginRight: 20,
    justifyContent: "center",
  },
});
