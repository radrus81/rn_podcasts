import React from "react";
import { StyleSheet, View } from "react-native";
import { AntDesign } from '@expo/vector-icons'
import { AppTextBold } from "../ui/AppTextBold";
import { AppButton } from "../ui/AppButton"
import { THEME } from "../../theme";

export const SubscribeButton = () => (
    <View style={styles.subscribe}>
        <View style={styles.wrapperSubscribe}>
            <AppButton style={styles.subscribeBtn}>
                <AntDesign name="plus" size={20} color={THEME.MAIN_COLOR} />
                <AppTextBold style={styles.textBtn}>
                    Подписаться
            </AppTextBold>
            </AppButton>
        </View>
    </View>
)

const styles = StyleSheet.create({
    subscribe: {
        marginTop: 15,
        marginBottom: 15,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderTopColor: '#ccc',
        borderBottomColor: "#ccc",
        paddingTop: 15,
        paddingBottom: 15
    },
    textBtn: {
        fontSize: 16
    },
    wrapperSubscribe: {
        width: 90,
        marginLeft: 20
    },
    iconSubscribeBtn: {
        marginRight: 15
    },
    subscribeBtn: {
        borderRadius: 20,
        fontSize: 20,
        borderWidth: 1,
        width: 180,
        borderColor: "#ccc",
        paddingVertical: 7,
    }
})