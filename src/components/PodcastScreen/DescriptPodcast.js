import React, { useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import { Entypo } from "@expo/vector-icons";
import { AppButton } from "../ui/AppButton";
import { THEME } from "../../theme";

export const DescriptPodcast = ({ description }) => {
  const [moreDescription, setMoreDescription] = useState(false);
  const countWord = description.split(" ").length;
  var textDescription = <Text style={styles.text}>{description}</Text>;

  if (countWord > 25 && !moreDescription) {
    textDescription = (
      <View>
        <Text style={styles.text} numberOfLines={4} ellipsizeMode="tail">
          {description}
        </Text>
        <View>
          <AppButton>
            <Entypo
              name="chevron-thin-down"
              size={30}
              color={THEME.GREY_COLOR}
              onPress={() => setMoreDescription(true)}
            />
          </AppButton>
        </View>
      </View>
    );
  }

  return <View style={styles.description}>{textDescription}</View>;
};

const styles = StyleSheet.create({
  text: {
    fontSize: 14,
    marginLeft: 20,
    marginRight: 20,
    fontFamily: "open-regular",
  },
  description: {
    flexDirection: "row",
  },
});
