import React from "react";
import { StyleSheet, Image, View } from "react-native";
import { AppText } from "../ui/AppText";
import { THEME } from "../../theme";

export const TitlePodcast = ({ topic, author, img }) => {

    return (
        <View style={styles.titleBlock}>
            <View style={styles.textTitle}>
                <AppText style={styles.namePodcast}>{topic}</AppText>
                <AppText style={styles.author}>{author}</AppText>
            </View>
            <Image source={{ uri: img }} style={styles.imagePodcast} />
        </View>
    )
}


const styles = StyleSheet.create({
    titleBlock: {
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: "center",
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20

    }, textTitle: {
        width: "65%"
    },
    namePodcast: {
        fontSize: 20,
        marginBottom: 10

    }, author: {
        color: THEME.MAIN_COLOR

    }, imagePodcast: {
        width: 100,
        height: 100,
        borderRadius: 10
    },
})