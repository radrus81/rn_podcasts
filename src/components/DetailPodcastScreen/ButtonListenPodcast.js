import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Foundation, Feather, Ionicons } from "@expo/vector-icons";
import { AppButton } from "../ui/AppButton";
import { AppTextBold } from "../ui/AppTextBold";
import { THEME } from "../../theme";

export const ButtonListenPodcast = ({}) => (
  <View style={styles.listenBtnBlock}>
    <View style={styles.wrapperlistenBtn}>
      <AppButton style={styles.listenBtn}>
        <Foundation name="play-circle" size={20} style={styles.iconListenBtn} />
        <AppTextBold style={styles.textBtn}>Слушать</AppTextBold>
      </AppButton>
      <TouchableOpacity style={styles.wrapperIconDownload}>
        <Feather name="download" size={20} style={styles.iconListenBtnDown} />
      </TouchableOpacity>
      <TouchableOpacity style={styles.wrapperIconDownload}>
        <Ionicons name="md-more" size={20} style={styles.iconListenBtnMore} />
      </TouchableOpacity>
    </View>
  </View>
);

const styles = StyleSheet.create({
  listenBtnBlock: {
    marginTop: 15,
    marginBottom: 15,
    paddingTop: 20,
    paddingBottom: 20,
  },
  wrapperlistenBtn: {
    flexDirection: "row",
    justifyContent: "space-between",
  },

  listenBtn: {
    borderRadius: 20,
    fontSize: 20,
    width: "92%",
    borderColor: "#ccc",
    paddingVertical: 7,
    backgroundColor: THEME.MAIN_COLOR,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  iconListenBtn: {
    color: THEME.WHITE_COLOR,
    marginRight: 10,
  },
  textBtn: {
    fontSize: 16,
    color: THEME.WHITE_COLOR,
    fontFamily: "open-regular",
  },
  wrapperIconDownload: {
    padding: 8,
    borderWidth: 1,
    borderBottomColor: THEME.GREY_DARK,
    borderRadius: 50,
    width: 40,
    justifyContent: "center",
  },
  iconListenBtnDown: {
    color: THEME.MAIN_COLOR,
  },
  iconListenBtnMore: {
    color: THEME.MAIN_COLOR,
    marginLeft: 9,
  },
});
