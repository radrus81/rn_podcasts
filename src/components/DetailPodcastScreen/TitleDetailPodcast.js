import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { AppText } from "../ui/AppText";
import { AppTextBold } from "../ui/AppTextBold";
import { THEME } from "../../theme";

export const TitleDetailPodcast = ({ namePodcast, title, time }) => (
  <View>
    <TouchableOpacity>
      <AppTextBold style={styles.namePodcast}>{namePodcast}</AppTextBold>
    </TouchableOpacity>
    <AppTextBold style={styles.topic}>{title}</AppTextBold>
    <AppText style={styles.time}>{`${time} (106.1 MB)`}</AppText>
  </View>
);

const styles = StyleSheet.create({
  namePodcast: {
    fontSize: 14,
    color: THEME.MAIN_COLOR,
  },
  topic: {
    fontSize: 22,
    marginTop: 10,
    marginBottom: 10,
    color: THEME.GREY_DARK,
  },
  time: {
    color: THEME.GREY_COLOR2,
  },
});
