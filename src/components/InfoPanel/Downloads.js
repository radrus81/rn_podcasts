import React from "react";
import {
    View,
    Text,
    StyleSheet,
} from "react-native";

export const Downloads = () => {
    return (
        <View style={styles.wrapper}>
            <Text style={styles.text}>Скаченных подкастов нет</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
    },
    text: {
        textAlign: "center",
    }
})