import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  Dimensions,
} from "react-native";
import {
  ScrollableTabView,
  DefaultTabBar,
  ScrollableTabBar,
} from "@valdio/react-native-scrollable-tabview";
import { NewPodcast } from "./NewPodcast";
import { Process } from "./Process";
import { Downloads } from "./Downloads";

export const InfoPanel = () => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>Для вас</Text>
      <ScrollableTabView
        initialPage={0}
        tabBarUnderlineStyle={styles.underline}
        showsHorizontalScrollIndicator={false}
        renderTabBar={() => <DefaultTabBar />}
      >
        <NewPodcast tabLabel="Новые серии" />
        <Process tabLabel="В процессе" />
        <Downloads tabLabel="Скачивания" />
      </ScrollableTabView>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    margin: 10,
    marginTop: 20,
    borderColor: "#ccc",
    borderWidth: 1,
    borderRadius: 5,
    height: 160,
  },
  title: {
    textAlign: "left",
    fontSize: 22,
    marginBottom: 10,
    marginLeft: 15,
    fontFamily: "open-regular",
  },
  tab: {
    backgroundColor: "#e91e63",
  },
  underline: {
    width: "26%",
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
    marginLeft: 12,
    marginRight: 5,
  },
});
