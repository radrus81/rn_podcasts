import React from "react";
import { View, Image, StyleSheet } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { AppText } from "../ui/AppText";
import { AppTextBold } from "../ui/AppTextBold";

export const Process = () => {
  return (
    <View style={styles.wrapper}>
      <Image
        source={{ uri: "https://pbs.twimg.com/media/EM9zBYbU0AY8ggi.jpg" }}
        style={styles.image}
      />
      <View style={styles.texts}>
        <AppTextBold>Выпуск № 173</AppTextBold>
        <AppText style={styles.text}>6 ч. назад - Осталось 5 минут</AppText>
      </View>
      <View style={styles.icon}>
        <MaterialCommunityIcons
          name="play-circle-outline"
          size={25}
          color="#303f9f"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: "row",
    alignItems: "center",
    margin: 10,
  },
  texts: {
    marginLeft: 10,
    marginRight: 10,
  },
  text: {
    fontSize: 12,
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 5,
  },
  icon: {
    flex: 1,
    alignItems: "flex-end",
    marginRight: 5,
  },
});
