import React from "react";
import { View, Text, StyleSheet, } from "react-native";

export const NewPodcast = () => {
    return (
        <View style={styles.wrapper}>
            <Text style={styles.text}>Подпишитесь на подкасты, чтобы узнавать о новых выпусках</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
    },
    text: {
        textAlign: "center",
    }
})