export const THEME = {
  MAIN_COLOR: "#303f9f",
  DANGER_COLOR: "#d81b60",
  GREY_COLOR: "#ccc",
  GREY_COLOR2: "#7C7D7F",
  GREY_DARK: "#414244",
  WHITE_COLOR: "#ffffff",
  BLACK_COLOR: "#000000",
};
